<!DOCTYPE html>
<?php
    require_once('GuestBookDB.php');
    require_once('GuestBook.php');
        				
    $userIsEmpty = false;					
    $emailIsEmpty = false;				
    $msgIsEmpty = false;
    $msgTooLong = false;
    $post = $_SERVER["REQUEST_METHOD"] == "POST";
    
    if ($post) {
        $data = ['user' => trim($_POST['user']), 'email' => trim($_POST['email']), 'message' => trim($_POST['message'])];
    }
    else{
        $data = ['user' => "", 'email' => "", 'message' => ""];
    }
        
    if ($data["user"] == "") {
        $userIsEmpty = true;
    }
    if ($data["email"] == "") {
        $emailIsEmpty = true;
    }
    if ($data["message"] == "") {
        $msgIsEmpty = true;
    }
    if (strlen($data["message"]) > 1000) {
        $msgTooLong = true;
    }

    if (!$userIsEmpty && !$emailIsEmpty && !$msgIsEmpty && !$msgTooLong) {
        GuestBookDB::getInstance()->Insert(new GuestBook($data["user"], $data["email"], $data["message"]));
        header('Location: index.php' );
        exit;
    }
?>
<html>
    <head>
        <link href="Resources//Styles.css" type="text/css" rel="stylesheet" media="all" />
        <meta charset="cp1251">
        <title></title>
    </head>
    <body>
        <H1>����� ���������� � �������� �����!</H1>
        <input type="button" value="�������� ������" OnClick="window.scrollTo(0, document.documentElement.scrollHeight)"> 
        <?php
        $msgArray = GuestBookDB::getInstance()->Select();
        
        $output = "";
        foreach ($msgArray as $value) {
            $output .= "<table>";
            $output .="<th class='author'>��������� #" . $value->getNumber(). ". " 
                    . $value->getName() . " [<a href=mailto:" . $value->getEmail() . ">Email</a>]</th>"
                    . "<th class='date'>" . $value->getDate()->format('j F Y G:i') . "</th>"
                    . "<tr><td colspan=2>" . nl2br($value->getMsg()) . "</td></tr>";
            $output .= "</table>";
        }
        echo $output;
        ?>

        <form id="post_comment" action="index.php" method="POST">
            <label>���*:</label><input type="text" name="user" value="<?php echo $data['user']; ?>"/><br/>
            <?php            
            if ($userIsEmpty && $post) { echo ("<p>����������, ������� ���!</p>"); }
            ?> 
            <label>E-mail*:</label><input type="email" name="email" value="<?php echo $data['email']; ?>"/><br/>
            <?php
            if ($emailIsEmpty && $post) { echo ("<p>����������, ������� e-mail!</p>"); }                
            ?>
            <textarea name="message" rows="5" maxlength="1000"><?php echo $data['message']; ?></textarea><br/>
            <?php
            if ($msgIsEmpty && $post) { echo ("<p>����������, ������� �����������!</p>"); }                
            if ($msgTooLong && $post) { echo ("<p>����������� ������ ���� �� ������ 1000 ��������!</p>" ); }                 
            ?>
            <input type="submit" value="�������� �����������"/>
        </form>
    </body>
</html>
