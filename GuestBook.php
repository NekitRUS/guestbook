<?php
class GuestBook {
    private $name; 
    private $email; 
    private $msg;
    private $date;
    private $number;

    public function __construct($_name, $_email, $_msg, $_date = NULL, $_number = -1) { 
        $this->name = $_name; 
        $this->email = $_email; 
        $this->msg = $_msg;
        date_default_timezone_set('Europe/Moscow');
        if ($_date === NULL){
            $this->date = new DateTime();
        }
        else{
            $this->date = $_date;
        }
        $this->number = $_number;
    } 

    public function getName(){ 
        return $this->name; 
    } 
    public function getEmail(){ 
        return $this->email; 
    } 
    public function getMsg(){ 
        return $this->msg; 
    }
    
    public function getDate(){ 
        return $this->date; 
    } 
    
    public function getNumber(){ 
        return $this->number; 
    } 
}
